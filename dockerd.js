var pm2     = require('pm2'),
    restify = require('restify');

pm2.connect(function(err) {
    if (err) throw err;
});


var server = restify.createServer();

server.get('/:version/containers/json', function(req, res, next) {
    // Get all processes running
    pm2.list(function(err, list) {
        if (err) return next(err);

        var response = list.map(function(p) {
            return {
                 "Id": p.pm_id,
                 "Names": [p.name],
                 "Image": p.pm2_env.pm_exec_path,
                 "Command": "",
                 "Created": Math.floor(p.pm2_env.created_at / 1000),
                 "Status": p.pm2_env.status,
                 "Ports": [],
                 "SizeRw": 0,
                 "SizeRootFs": 0
            };
        });

        res.send(response);
    });
});

server.listen(8081, function() {
  console.log('%s listening at %s', server.name, server.url);
});
